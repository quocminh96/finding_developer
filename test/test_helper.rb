# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

# Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
class ActiveSupport
  class TestCase
    fixtures :all
    # Add more helper methods to be used by all tests here...
  end
end
