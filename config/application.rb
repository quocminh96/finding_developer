# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FindingDeveloper
  # Initialize configuration defaults for originally generated Rails version.
  class Application < Rails::Application
    config.load_defaults 5.2

    config.generators.system_tests = nil
    config.generators do |g|
      g.test_framework :rspec,
                       view_specs: false,
                       helper_specs: false,
                       routing_specs: false,
                       controller_specs: false,
                       request_specs: false
      g.javascripts false
      g.stylesheets false
    end
  end
end
