# frozen_string_literal: true

# Config ApplicationRecord
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
