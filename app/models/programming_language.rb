# frozen_string_literal: true

# Table ProgrammingLanguage
class ProgrammingLanguage < ApplicationRecord
  has_and_belongs_to_many :developers
end
