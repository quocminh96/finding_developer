# frozen_string_literal: true

# Table Developer
class Developer < ApplicationRecord
  has_and_belongs_to_many :programming_languages
  has_and_belongs_to_many :languages
end
