# frozen_string_literal: true

# Create table Language
class CreateLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :languages do |t|
      t.string :code, null: false

      t.timestamps
    end

    add_index :languages, :code
  end
end
