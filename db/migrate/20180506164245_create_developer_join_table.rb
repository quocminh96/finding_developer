class CreateDeveloperJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :languages, :developers do |t|
      t.index :language_id
      t.index :developer_id
    end

    create_join_table :programming_languages, :developers do |t|
      t.index :programming_language_id, name: 'index_dev_p_lang_on_lang_id'
      t.index :developer_id, name: 'index_dev_p_lang_on_dev_id'
    end
  end
end
