# frozen_string_literal: true

# Create table ProgrammingLanguage
class CreateProgrammingLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :programming_languages do |t|
      t.string :name, null: false

      t.timestamps
    end

    add_index :programming_languages, :name, unique: true
  end
end
