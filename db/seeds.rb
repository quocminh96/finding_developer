# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

developer_list = [
  "minhle@teqnological.asia",
  "minhluong@teqnological.asia",
  "ngocnguyen@teqnological.asia",
  "tuannguyen@teqnological.asia",
  "hieuhoang@teqnological.asia",
  "khoathai@teqnological.asia",
  "namle@teqnological.asia"
]

developer_list.each do |email|
  Developer.create( email: email )
end

programming_list = [
  "Ruby",
  "Python",
  "C",
  "C++",
  "C#",
  "Java",
  "Swift",
  "Golang",
  "ReactJS",
  "AngularJS"
]

programming_list.each do |name|
  ProgrammingLanguage.create( name: name )
end

language_list = [
  "eng",
  "vie",
  "jpn",
  "chi",
  "tha",
  "ger",
  "rus",
  "may",
  "kor"
]

language_list.each do |code|
  Language.create( code: code )
end