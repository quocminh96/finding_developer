## Local development

### Required software

* Ruby 2.5.0
* Ruby on Rails 5.2.0
* MYSQL

### Database setup

```shell
rails db:setup
rails db:migrate
```

### Run Specs (Tests)

```shell
bundle exec rspec
```
